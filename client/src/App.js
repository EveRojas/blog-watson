import React, {Component} from 'react';
import axios from 'axios';
import './App.scss';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Home from './components/home'
import News from './components/news'
import CityLife from './components/citylife'
import ScienceNote from './components/sciencenote'

import FooterBlog from './footer'


class App extends Component{
  constructor(props){
    super(props)

    axios
    .get('/posts')
    .then(result => {
      console.log(result)
    })

     axios
    .get('/comments/teste')
    .then(result => {
      console.log(result)
    })


  }

render () {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/news">News</Link>
            </li>
            <li>
              <Link to="/citylife">CityLife</Link>
            </li>
             <li>
              <Link to="/sciencenote">ScienceNote</Link>
            </li>


          </ul>
        </nav>
        <Switch>
          <Route path="/news">
            <News/>
          </Route>
          <Route path="/citylife">
            <CityLife/>
          </Route>
          <Route path="/sciencenote">
            <ScienceNote/>
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
        <FooterBlog/>
      </div>
    </Router>
  );
 }
} 
export default App;
