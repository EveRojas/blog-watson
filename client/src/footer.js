import React from 'react'


const FooterBlog = props =>{

	return(
        <div className='footer-container'>

	        <div className='footer-module-grid'>

		        <div className='footer-col'>
			
			        <div className='footer-row'>

				        <ul className='footer-module-nav'>
                            <li className='footer-module-item'>
                            <a href="#" aria-label="Contribute">Contribute</a>
                            </li>
                            <li className='footer-module-item'>
                            <a href="#" aria-label="Privacy">Privacy</a>
                            </li>
                            <li className='footer-module-item'>
                            <a href="#" aria-label="Terms">Terms of Use</a>
                            </li>

				        </ul>
			        </div>
		        </div>
	        </div>

        </div>


		)
}

export default FooterBlog;