const express = require('express')
const router = express.Router()

router.get('/comments/:postid', (req, res) => {
	res.json({
		content: 'testing comments',
		author: 'Eve Rojas'	
	})
})


module.exports = router
